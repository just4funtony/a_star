using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Field
{
    private int width;
    public int Width => width;
    private int height;
    public int Height => height;

    private PathNode[,] pathNodes;
    public PathNode[,] PathNodes => pathNodes;

    private List<Cell> cells = new List<Cell>();
    public List<Cell> Cells => cells;

    public Field(float offcet, int width, int height)
    {
        this.width = width;
        this.height = height;
        pathNodes = new PathNode[width, height];
        Cell cellPrefab = Resources.Load<Cell>("Cell/Cell");//prefab
        for (int x = 0; x < pathNodes.GetLength(0); x++)
        {
            for (int y = 0; y < pathNodes.GetLength(1); y++)
            {
                pathNodes[x, y] = new PathNode(this , x, y);
                var cell = MonoBehaviour.Instantiate(cellPrefab, new Vector2(x * offcet, y * offcet), Quaternion.identity);
                cell.InitCell(pathNodes[x, y]);
                cells.Add(cell);
            }
        }
    }

    public PathNode GetFieldObject(int x, int y)
    {
        if (x >= 0 && y >= 0 && x < width && y < height)
        {
            return pathNodes[x, y];
        }
        else
        {
            return default(PathNode);
        }
    }
}
