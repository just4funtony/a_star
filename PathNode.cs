using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PathNode
{
    private static List<PathNode> neighbourList;
    public List<PathNode> NeighbourList => neighbourList;

    private UnitComponent unitInCell;
    public UnitComponent UnitInCell => unitInCell;

    private Cell cell;
    public Cell Cell => cell;

    private Field field;

    private int x;
    public int X => x;

    private int y;
    public int Y => y;

    public int gCost;
    public int hCost;
    public int fCost;

    public PathNode came_from;
    public void InitNode(Cell cell)
    {
        this.cell = cell;
        neighbourList = getNeighbourList();
    }
    public PathNode(Field field, int x, int y)
    {
        this.field = field;
        this.x = x;
        this.y = y;
    }
    private List<PathNode> getNeighbourList()
    {
        List<PathNode> n = new List<PathNode>();
        if (x - 1 >= 0)
        {
            n.Add(GetNode(x - 1, y));
            if (y - 1 >= 0) n.Add(GetNode(x - 1, y - 1));
            if (y + 1 < field.Height) n.Add(GetNode(x - 1, y + 1));
        }
        if (x + 1 < field.Width)
        {
            n.Add(GetNode(x + 1, y));
            if (y - 1 >= 0) n.Add(GetNode(x + 1, y - 1));
            if (y + 1 < field.Height) n.Add(GetNode(x + 1, y + 1));
        }
        if (y - 1 >= 0) n.Add(GetNode(x, y - 1));
        if (y + 1 < field.Height) n.Add(GetNode(x, y + 1));
        return n;
    }
    private PathNode GetNode(int x, int y)
    {
        return field.GetFieldObject(x, y);
    }
    private int calculateDistadceCost(PathNode a, PathNode b)
    {
        int xDistance = Mathf.Abs(a.X - b.X);
        int yDistance = Mathf.Abs(a.Y - b.Y);
        int remaining = Mathf.Abs(xDistance - yDistance);
        return 14 * Mathf.Min(xDistance, yDistance) + 10 * remaining;
    }
    public void CalculateFCost()
    {
        fCost = gCost + hCost;
    }
}
