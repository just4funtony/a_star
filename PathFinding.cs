using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinding
{
    private const int MOVE_COST = 10;
    private const int MOVE_DIAGONAL_COST = 14;
    public Field Field => field;
    private Field field;
    private List<PathNode> openList;
    private List<PathNode> closedList;
    public PathFinding(float offcet, int width, int height, Transform parentTransform)
    {
        field = new Field(offcet, width, height);
        foreach(var i in field.Cells)
        {
            i.PathNode.InitNode(i);
            i.transform.parent = parentTransform;
        }
    }
    public List<PathNode> FindPath(int startX, int startY, int endX, int endY)
    {
        PathNode startNode = field.GetFieldObject(startX, startY);
        PathNode endNode = field.GetFieldObject(endX, endY);

        if (startNode == null || endNode == null)
        {
            return null;
        }

        openList = new List<PathNode> { startNode };
        closedList = new List<PathNode>();
        for (int x = 0; x < field.Width; x++)
        {
            for (int y = 0; y < field.Height; y++)
            {
                PathNode pathNode = field.GetFieldObject(x, y);
                pathNode.gCost = int.MaxValue;
                pathNode.CalculateFCost();
                pathNode.came_from = null;
            }
        }

        startNode.gCost = 0;
        startNode.hCost = calculateDistadceCost(startNode, endNode);
        startNode.CalculateFCost();
        while (openList.Count > 0)
        {
            PathNode currentNode = getLowestFCostNode(openList);
            if (currentNode == endNode)
            {
                return CalculatePath(endNode);
            }
            openList.Remove(currentNode);
            closedList.Add(currentNode);

            foreach (PathNode neighbourNode in currentNode.NeighbourList)
            {
                if (closedList.Contains(neighbourNode)///
                    ) continue;
                int tentativeGCost = currentNode.gCost + calculateDistadceCost(currentNode, neighbourNode);
                if (tentativeGCost < neighbourNode.gCost)
                {
                    neighbourNode.came_from = currentNode;
                    neighbourNode.gCost = tentativeGCost;
                    neighbourNode.hCost = calculateDistadceCost(neighbourNode, endNode);
                    neighbourNode.CalculateFCost();

                    if (!openList.Contains(neighbourNode))
                    {
                        openList.Add(neighbourNode);
                    }
                }
            }
        }
        return null;
    }

    private int calculateDistadceCost(PathNode a, PathNode b)
    {
        int xDistance = Mathf.Abs(a.X - b.X);
        int yDistance = Mathf.Abs(a.Y - b.Y);
        int remaining = Mathf.Abs(xDistance - yDistance);
        return MOVE_DIAGONAL_COST * Mathf.Min(xDistance, yDistance) + MOVE_COST * remaining;
    }
    private PathNode getLowestFCostNode(List<PathNode> pathNodeList)
    {
        PathNode lowestFCostNode = pathNodeList[0];
        for (int i = 1; i < pathNodeList.Count; i++)
        {
            if (pathNodeList[i].fCost < lowestFCostNode.fCost)
            {
                lowestFCostNode = pathNodeList[i];
            }
        }
        return lowestFCostNode;
    }
    private List<PathNode> CalculatePath(PathNode endNode)
    {
        List<PathNode> path = new List<PathNode>();
        path.Add(endNode);
        PathNode currentNode = endNode;
        while (currentNode.came_from != null)
        {
            path.Add(currentNode.came_from);
            currentNode = currentNode.came_from;
        }
        path.Reverse();
        return path;
    }
}
